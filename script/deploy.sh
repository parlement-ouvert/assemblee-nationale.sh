#!/bin/bash

if [[ -z "${CI}" ]]; then
    echo "This script must run in the CI environment." 1>&2
    exit 1
fi

ssh root@ssh.assemblee-nationale.sh /bin/bash << EOF
    cd assemblee-nationale.sh
    git pull
    docker build . -t assemblee-nationale.sh:${CI_PIPELINE_ID}
    if [ ! "$(docker ps -q -f name=assemblee-nationale.sh)" ]; then
        if [ ! "$(docker ps -aq -f status=exited -f name=assemblee-nationale.sh)" ]; then
            docker stop assemblee-nationale.sh
        fi
        docker rm -f assemblee-nationale.sh
    fi
    docker run --restart=always -dit -p 5000:5000 --name=assemblee-nationale.sh assemblee-nationale.sh:${CI_PIPELINE_ID}
EOF
