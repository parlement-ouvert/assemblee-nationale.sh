FROM python:3.6.5-alpine3.7

RUN apk update && \
    apk add libxml2-dev libxslt-dev libc-dev gcc

RUN pip install --upgrade pip && \
    pip install gunicorn lxml requests && \
    pip install --no-binary :all: falcon

EXPOSE 5000

RUN mkdir -p /srv/app
COPY --chown=root:root main.py /srv/app/
COPY --chown=root:root template /srv/app/template

WORKDIR /srv/app

ENTRYPOINT ["gunicorn", "-b", "0.0.0.0:5000", "main:app"]
