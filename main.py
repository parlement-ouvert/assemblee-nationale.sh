#!/usr/bin/env python3
# -*- coding=utf-8 -*-

import requests
import re
import html
import falcon

from lxml import etree
from io import StringIO

class FileResolver(etree.Resolver):
    def resolve(self, url, pubid, context):
        return self.resolve_filename(url, context)

def xsl_response(stylesheet):
    def f(req, res):
        content = None
        urls = [
            'http://www.assemblee-nationale.fr' + req.relative_uri,
            'http://www2.assemblee-nationale.fr' + req.relative_uri,
        ]

        for url in urls:
            an_resp = requests.get(url)
            if an_resp.status_code == 200:
                content = an_resp.text
                break

        if content == None:
            res.status_code = 404
        else:
            # When the URL contains '(ajax)', it means we're loading an HTML fragment. Not a whole page.
            if '(ajax)' in req.relative_uri:
                content = '<body>' + html.unescape(content) + '</body>'
            else:
                m = re.search(r"[\s\S]*<body>([.\s\S]*)</body>[\s\S]*", content, re.MULTILINE)
                content = '<body>' + html.unescape(m.group(1)) + '</body>'
            
            # FIXME: use the HTML or HTML5 parsers?
            # http://lxml.de/html5parser.html
            # http://lxml.de/parsing.html
            parser = etree.XMLParser(ns_clean=True, recover=True, remove_blank_text=True)
            parser.resolvers.add(FileResolver())
            tree = etree.parse(StringIO(content), parser)

            xslt_root = etree.parse(open(stylesheet, 'r'), parser)
            transform = etree.XSLT(xslt_root)
            result_tree = transform(tree)

            res.content_type = 'application/xml'
            res.data = etree.tostring(result_tree.getroot(), pretty_print=True) 

    return f

app = falcon.API()
app.add_sink(
    xsl_response('template/documents/liste/type/propositions-loi.xsl'),
    '/documents/liste/\(type\)/propositions-loi'
)
app.add_sink(
    xsl_response('template/documents/liste/type/projets-loi.xsl'),
    '/documents/liste/\(type\)/projets-loi'
)
app.add_sink(
    xsl_response('template/documents/liste/type/ta.xsl'),
    '/documents/liste/\(type\)/ta'
)
app.add_sink(
    xsl_response('template/documents/liste/type/rapports-application-loi.xsl'),
    '/documents/liste/\(type\)/rapports-application-loi'
)
app.add_sink(
    xsl_response('template/documents/liste/type/rapports-information.xsl'),
    '/documents/liste/\(type\)/rapports-information'
)
app.add_sink(
    xsl_response('template/documents/liste/type/rapports.xsl'),
    '/documents/liste/\(type\)/rapports'
)
app.add_sink(
    xsl_response('template/documents/notice/legislature/projets/id/index/projets-loi.xsl'),
    '/documents/notice/'
)
app.add_sink(
    xsl_response('template/legislature/dossiers.xsl'),
    '/[0-9]{2}/dossiers/'
)
