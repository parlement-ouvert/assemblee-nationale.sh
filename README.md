# assemblee-nationale.sh

An URL-based drop-in replacement to turn [the Assemblée Nationale website](http://www.assemblee-nationale.fr) into an XML REST API.

## Motivation

The official open data from the Assemblée Nationale website is hard to understand/work with.
It's much easier to be able to get XML data from every existing actual web page.

The inspiration is taken from [nosdeputes.fr](https://www.nosdeputes.fr) by [RegardsCitoyens](https://www.regardscitoyens.org/), which
allows to turn any web page into an XML document by adding `/xml` at the end of the URL ([example](https://www.nosdeputes.fr/deputes/xml)).

## How to use

Take any [supported URL](#features) on [the Assemblée Nationale website](http://www.assemblee-nationale.fr) and replace the `.fr` TLD with `.sh`.

## Demo

* Bills list
  * Web page: http://www2.assemblee-nationale.fr/documents/liste/(type)/projets-loi
  * XML: http://www2.assemblee-nationale.sh/documents/liste/(type)/projets-loi

## Development

Requirements:

* Docker 18+

```
docker build . -t assemblee-nationale.sh:latest
docker run --rm -p 5000:5000 -v $PWD:/srv/app assemblee-nationale.sh:latest --reload
```

## Features

* Supported URLs:
  * `/documents/liste/(type)/propositions-loi`
  * `/documents/liste/(type)/projets-loi`
  * `/documents/liste/(type)/ta`
  * `/documents/liste/(type)/rapports-application-loi`
  * `/documents/liste/(type)/rapports-information`
  * `/documents/liste/(type)/rapports`
  * `/documents/notice/*`
  * `/{legislature}/dossiers/*`
