<xsl:stylesheet
    version="1.0"
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

    <xsl:output method="xml" indent="yes"/>

    <xsl:template match="a[starts-with(@href, '/commissions/')]">
        <commision name="{text()}" uri="{@href}"/>
    </xsl:template>

    <xsl:template match="a[starts-with(@href, '/amendements/')]">
        <xsl:variable name="description" select="text()"/>

        <amendments uri="{@href}">
            <xsl:if test="$description">
                <description>
                    <xsl:value-of select="$description"/>
                </description>
            </xsl:if>
        </amendments>
    </xsl:template>

    <xsl:template match="a[substring(@href, 4, 5) = '/cri/']">
        <xsl:variable name="description">
            <xsl:for-each select="text()|node()">
                <xsl:value-of select="."/>
            </xsl:for-each>
        </xsl:variable>

        <document type="cri" uri="{@href}">
            <description>
                <xsl:value-of select="normalize-space($description)"/>
            </description>
        </document>
    </xsl:template>

    <xsl:template match="a[substring(@href, 4, 4) = '/ta/']">
        <document type="ta" uri="{@href}"/>
    </xsl:template>

    <xsl:template match="a[substring(@href, 4, 9) = '/cr-cloi/']">
        <document type="cr-cloi" uri="{@href}" date="{normalize-space(text())}">
            <description>
                <xsl:value-of select="normalize-space(substring-before(preceding-sibling::text(), ' au cours de la réunion du'))"/>
            </description>
        </document>
    </xsl:template>

    <xsl:template match="a[substring(@href, 4, 10) = '/rapports/']">
        <document
            type="report"
            uri="{@href}"
            date="{substring-before(substring-after(following-sibling::text(), 'mis en ligne le '), ')')}">
        </document>
    </xsl:template>

    <xsl:template match="a[substring(@href, 4, 15) = '/ta-commission/']">
        <document
            type="ta-commission"
            uri="{@href}"
            date="{substring-before(substring-after(following-sibling::text(), 'mis en ligne le '), ')')}">
        </document>
    </xsl:template>

    <xsl:template match="a[starts-with(@href, 'http://www.senat.fr/leg/pjl')]">
        <document type="pjl" uri="{@href}"/>
    </xsl:template>

    <xsl:template match="a[starts-with(@href, 'http://www.senat.fr/senateur/')]">
        <person type="senator" uri="{@href}" name="{text()}"/>
    </xsl:template>

    <xsl:template match="a[starts-with(@href, 'http://www.senat.fr/rap/')]">
        <document
            type="report"
            uri="{@href}"
            date="{normalize-space(substring-before(substring-after(following-sibling::text(), 'déposé le '), ' :'))}"/>
    </xsl:template>

    <xsl:template match="a"></xsl:template>

    <xsl:template match="/body">
        <response>
            <data>
                <xsl:apply-templates select="//a"/>
            </data>
        </response>
    </xsl:template>

</xsl:stylesheet>
