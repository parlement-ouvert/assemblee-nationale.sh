<xsl:stylesheet
    version="1.0"
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

    <xsl:output method="xml" indent="yes"/>

    <xsl:template match="@href" priority="10">
        <xsl:value-of select="substring-after(., 'assemblee-nationale.fr')"/>
    </xsl:template>

    <xsl:template match="/body">
        <xsl:variable name="previous" select=".//div[@class='bottommargin pagination-bootstrap pagination-right pagination-mini']/ul/li/a[span/text() = '&#171;&#160;Pr&#233;c&#233;dent']/@href"/>
        <xsl:variable name="next" select=".//div[@class='bottommargin pagination-bootstrap pagination-right pagination-mini']/ul/li/a[span/text() = 'Suivant&#160;&#187;']/@href"/>

        <response>
            <data>
                <documents>
                    <xsl:apply-templates select="//ul[@class='liens-liste']"/>
                </documents>
            </data>
            <xsl:if test="$previous or $next">
                <paging>
                    <xsl:if test="$previous">
                        <xsl:attribute name="previous">
                            <xsl:value-of select="$previous"/>
                        </xsl:attribute>
                    </xsl:if>
                    <xsl:if test="$next">
                        <xsl:attribute name="next">
                            <xsl:value-of select="$next"/>
                        </xsl:attribute>
                    </xsl:if>
                </paging>
            </xsl:if>
        </response>
    </xsl:template>

    <xsl:template match="ul[@class='liens-liste']">
        <xsl:apply-templates match="./li"/>
    </xsl:template>

    <xsl:template name="format-date">
        <xsl:param name="date"/>

        <xsl:variable name="day-of-week" select="substring-before($date, ' ')"/>
        <xsl:variable name="day" select="substring-before(substring-after($date, concat($day-of-week, ' ')), ' ')"/>
        <xsl:variable name="month-name" select="substring-before(substring-after($date, concat($day, ' ')), ' ')"/>
        <xsl:variable name="year" select="substring-before(substring-after($date, concat($month-name, ' ')), ' ')"/>
        <xsl:variable name="time" select="substring-after($date, '&#224; ')"/>

        <xsl:variable name="month">
            <xsl:choose>
                <xsl:when test="$month-name = 'janvier'">
                    <xsl:text>01</xsl:text>
                </xsl:when>
                <xsl:when test="$month-name = 'février'">
                    <xsl:text>02</xsl:text>
                </xsl:when>
                <xsl:when test="$month-name = 'mars'">
                    <xsl:text>03</xsl:text>
                </xsl:when>
                <xsl:when test="$month-name = 'avril'">
                    <xsl:text>04</xsl:text>
                </xsl:when>
                <xsl:when test="$month-name = 'mai'">
                    <xsl:text>05</xsl:text>
                </xsl:when>
                <xsl:when test="$month-name = 'juin'">
                    <xsl:text>06</xsl:text>
                </xsl:when>
                <xsl:when test="$month-name = 'juillet'">
                    <xsl:text>07</xsl:text>
                </xsl:when>
                <xsl:when test="$month-name = 'août'">
                    <xsl:text>08</xsl:text>
                </xsl:when>
                <xsl:when test="$month-name = 'septembre'">
                    <xsl:text>09</xsl:text>
                </xsl:when>
                <xsl:when test="$month-name = 'octobre'">
                    <xsl:text>10</xsl:text>
                </xsl:when>
                <xsl:when test="$month-name = 'novembre'">
                    <xsl:text>11</xsl:text>
                </xsl:when>
                <xsl:when test="$month-name = 'décembre'">
                    <xsl:text>12</xsl:text>
                </xsl:when>
            </xsl:choose>
        </xsl:variable>
        
        <xsl:if test="$year and $month and $day and $time">
            <xsl:value-of select="$year"/>/<xsl:value-of select="$month"/>/<xsl:value-of select="$day"/>
            <xsl:text> </xsl:text>
            <xsl:value-of select="translate($time, 'h', ':')"/>
        </xsl:if>

    </xsl:template>

    <xsl:template match="ul[@class='liens-liste']/li">
        <xsl:variable name="title" select="./h3/text()"/>
        <xsl:variable name="date">
            <xsl:value-of select="substring-after(./ul/li[1]/span[@class='heure']/text(), ' Mis en ligne ')"/>
        </xsl:variable>
        <xsl:variable name="folder" select="./ul/li[a][1]/a/@href"/>

        <document>
            <xsl:attribute name="id">
                <xsl:value-of select="substring-after($title, 'N&#176;&#160;')"/>
            </xsl:attribute>
            <xsl:attribute name="date">
                <xsl:call-template name="format-date">
                    <xsl:with-param name="date" select="$date"/>
                </xsl:call-template>
            </xsl:attribute>
            <xsl:attribute name="legislature">
                <xsl:value-of select="substring-before(substring-after($folder, 'assemblee-nationale.fr/'), '/dossier')"/>
            </xsl:attribute>
            <xsl:attribute name="folder">
                <xsl:apply-templates select="$folder"/>
            </xsl:attribute>
            <xsl:attribute name="text">
                <xsl:value-of select="./ul/li[3]/a/@href"/>
            </xsl:attribute>
            <xsl:attribute name="title">
                <xsl:value-of select="normalize-space($title)"/>
            </xsl:attribute>

            <description>
                <xsl:value-of select="./p"/>
            </description>
        </document>
    </xsl:template>


</xsl:stylesheet>
