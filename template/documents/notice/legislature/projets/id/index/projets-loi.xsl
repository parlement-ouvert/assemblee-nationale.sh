<xsl:stylesheet
    version="1.0"
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

    <xsl:output method="xml" indent="yes" cdata-section-elements="content"/>

    <xsl:template match="@*|node()">
        <xsl:copy>
            <xsl:apply-templates select="@*|node()"/>
        </xsl:copy>
    </xsl:template>

    <xsl:template match="span|b|i|br|hr|table">
        <xsl:value-of select="text()"/>
    </xsl:template>

    <xsl:template match="p">
        <xsl:apply-templates select="*"/>
        <xsl:text>
</xsl:text>
    </xsl:template>

    <xsl:template match="div|ul|li|a">
        <xsl:apply-templates select="*"/>
    </xsl:template>

    <xsl:template match="/body">
        <xsl:variable name="title" select="//div/div/ol[@class='breadcrumb']/li[5]/text()"/>
        <xsl:variable name="path" select="//div/div/ol[@class='breadcrumb']/li[4]/a/@href"/>

        <response>
            <data>
                <document>
                    <xsl:attribute name="title">
                        <xsl:value-of select="$title"/>
                    </xsl:attribute>
                    <xsl:attribute name="legislature">
                        <xsl:value-of select="substring-after($path, '/documents/liste/(type)/projets-loi/(legis)/')"/>
                    </xsl:attribute>
                    <xsl:attribute name="id">
                        <xsl:value-of select="substring-before(substring-after($title, 'N&#176;&#160;'), ' - ')"/>
                    </xsl:attribute>

                    <content>
                        <xsl:apply-templates select="//article/*"/>
                    </content>
                </document>
            </data>
        </response>
    </xsl:template>

</xsl:stylesheet>
